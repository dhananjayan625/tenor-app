const prepareData = (data = [], columnCount = 4) => {
  let noOfColumns = columnCount;
  let column1 = [],
    column2 = [],
    column3 = [],
    column4 = [];
  for (let i = 0; i < data.length; i += noOfColumns) {
    const chunk = data.slice(i, i + noOfColumns);
    let num = [0, 1, 2, 3];
    if (chunk[0]) column1.push(chunk[num[0]]);
    if (chunk[1]) column2.push(chunk[num[1]]);
    if (chunk[2]) column3.push(chunk[num[2]]);
    if (chunk[3]) column4.push(chunk[num[3]]);
  }
  return [column1, column2, column3, column4];
};

export default prepareData;
