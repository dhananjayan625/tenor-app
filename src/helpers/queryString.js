const stringify = (data) => {
  if (typeof data !== "object") return ""; // Check the given parameter is object
  let queryString = "?";
  Object.entries(data).map((item, index) => {
    // Remove the null item
    if (item[1] != null)
      queryString += `${index > 0 ? "&" : ""}${item[0]}=${encodeURIComponent(item[1])}`;

      return null;
  });
  return queryString;
};

const queryString = {
  stringify,
};

export default queryString;
