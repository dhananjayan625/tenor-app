export const calculateColumnCount = () => {
    if(typeof window === "undefined") return 4;
  if (window.innerWidth < 992) return 2;
  if (window.innerWidth < 1200) return 3;
  if (window.innerWidth > 1200) return 4;
};
