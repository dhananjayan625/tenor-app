import config from "../config";
import queryString from "./queryString";

// let client_key = "TENOR_APP_TASK";
// const apikey = "AIzaSyB0SN5B9k9sZ7E1j_5tvRg24glFnScSlqY";

const httpGetPromise = (url) => {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();

    xhr.open("GET", url, true);

    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        let response = JSON.parse(xhr.response);
        resolve(response);
      } else {
        reject(xhr.statusText);
      }
    };
    xhr.onerror = () => reject(xhr.statusText);
    xhr.send(null);
  });
};

export const getFeaturedData = async (data, callback) => {
  const baseUrl = config.featuredBaseUrl;
  const apikey = config.tenorApiKeyOne;
  const { limit = 8, searchTerm, pos } = data || {};
  let query = queryString.stringify({
    limit,
    key: apikey,
    searchTerm,
    pos,
    client_key: config.tenorApiKeyClient,
  });
  let searchUrl = `${baseUrl}/${query}`;
  try {
    
  const featuredData = await httpGetPromise(searchUrl);
  let results = [];
  if (featuredData && Array.isArray(featuredData.results))
    results = featuredData.results.map((item, index) => {
      const { url, dims } = item?.media_formats?.nanogif || {};
      return {
        id: item?.id,
        url,
        dims,
      };
    });
  return { results, next: featuredData.next };
  } catch (error) {
    return {}
  }
};

export const getTrendingData = async (data) => {
  const baseUrl = config.trendingTermbaseUrl;
  const apikey = config.tenorApiKeyOne;
  let query = queryString.stringify({ key: apikey, client_key: config.tenorApiKeyClient, ...data });
  let searchUrl = `${baseUrl}/${query}`;
  const trendingData = await httpGetPromise(searchUrl);
  let results = [];
  if (trendingData && Array.isArray(trendingData.results))
    results = trendingData.results.map((item, index) => ({
      id: index,
      trendingTerm: item,
    }));
  return results;
};

export const getImageForTrendingTerms = async (trendingTerms) => {
  const results = [];
  for (let i = 0; i < trendingTerms?.length; i++) {
    const trendingTerm = trendingTerms?.[i]?.trendingTerm;
    const imageData = await getGifBySearchTerm({ q: trendingTerm, limit: 1 });
    let obj = { trendingTerm };
    obj.imageSrc = imageData?.results?.[0]?.media?.[0]?.nanogif?.url;
    results.push(obj);
  }
  return results;
};

export const getImageForTrendingTerm = async (data) => {
  const trendingTerm = data?.trendingTerm;
  const imageData = await getGifBySearchTerm({ q: trendingTerm, limit: 1 });
  let obj = { ...data };
  obj.imageSrc = imageData?.results?.[0]?.url;
  return obj;
};

export const getGifBySearchTerm = async (data, callback) => {
  const apikey = config.tenorApiKeyTwo;
  const baseUrl = config.searchBaseUrl;
  let query = queryString.stringify({
    media_filter: "basic",
    key: apikey,
    client_key: config.tenorApiKeyClient,
    ...data,
  });
  let searchUrl = `${baseUrl}/${query}`;
  const searchData = await httpGetPromise(searchUrl);
  let results = [];
  if (searchData && Array.isArray(searchData.results))
    results = searchData.results.map((item, index) => {
      const { url, dims } = item?.media?.[0]?.nanogif || {};
      return {
        id: item?.id,
        dims,
        url,
      };
    });
  return { results, next: searchData.next };
};

export const getImageForTerm = (data) =>
  new Promise((resolve, reject) => {
    getGifBySearchTerm(data, resolve);
  });
