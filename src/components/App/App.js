import { lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";

import Header from "../Header/Header";

import "./App.css";

const Home = lazy(() => import("../Home/Home"));
const Search = lazy(() => import("../Search/Search"));


export default function App() {
  return (
    <div>
      <Header />
      <main>
        <Suspense >
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/search" element={<Search />} />
            <Route path="/search/:searchTerm" element={<Search />} />
          </Routes>
        </Suspense>
      </main>
    </div>
  );
}
