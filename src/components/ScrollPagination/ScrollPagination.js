import { useCallback, useEffect, useRef } from "react";
import PropTypes from "prop-types";

import LazyImage from "../LazyImage/LazyImage";

import "./ScrollPagination.css";

function ScrollPagination(props) {
  const { loadNext, results = [], isFeatured } = props;
  const loaderRef = useRef();

  const handleObserver = useCallback((entries) => {
    const target = entries[0];
    if (target.isIntersecting) {
      loadNext();
    }
  }, []);

  useEffect(() => {
    const option = {
      root: null,
      rootMargin: "800px",
      threshold: 0,
    };
    const observer = new IntersectionObserver(handleObserver, option);
    if (loaderRef.current) observer.observe(loaderRef.current);
  }, [handleObserver]);


  return (
    <div>
      <h2 className={`mb-3 ${isFeatured ? "mt-4" : ""}`}>
        {isFeatured ? "Featured GIFs" : "GIFs"}
      </h2>
      <div className="scrollPaginationContainer">
        {results.map((columnData, columnIndex) => {
          if (Array.isArray(columnData) && columnData.length > 0)
            return (
              <div
                key={`SEARCH_RESULT_COLUMN_${columnIndex}`}
                className="scrollColumn"
              >
                {columnData.map((item, index) => {
                  let { url, id } = item || {};
                  return (
                    <div
                      key={`SEARCH_ITEM_COLUMN_${
                        columnIndex + index
                      }_ITEM_${id}`}
                    >
                      <LazyImage
                        keyValue={`SEARCH_ITEM_COLUMN_${
                          columnIndex + index
                        }_ITEM_IMG_${id}`}
                        src={url}
                        alt="Image"
                        className="gif-image"
                      />
                    </div>
                  );
                })}
              </div>
            );
        })}
      </div>
      <div ref={loaderRef} />
    </div>
  );
}

ScrollPagination.propTypes = {
  loadNext: PropTypes.func,
  results: PropTypes.array,
  isFeatured: PropTypes.bool,
};

export default ScrollPagination;
