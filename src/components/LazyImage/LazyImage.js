import React, { useEffect, useRef, useState } from "react";
import PropTypes from 'prop-types';

function LazyImage(props) {
  const { src, className, keyValue } = props;
  const [isLoaded, setIsLoaded] = useState(false);
  const imageRef = useRef();

  const handleObserve = (entries, observer) => {
    const [entry] = entries;
    if (entry.isIntersecting) {
      observer.unobserve(imageRef.current);
      let image = entry.target;
      let testImage = new Image();
      testImage.src = src;
      testImage.onload = () => {
        image.src = src;
        setIsLoaded(true);
      };
    }
  };

  useEffect(() => {
    const observer = new IntersectionObserver(handleObserve);
    if (!isLoaded) {
      if (imageRef.current) observer.observe(imageRef.current);
    }

    return () => {
      if (imageRef.current) observer.unobserve(imageRef.current);
    };
  }, [imageRef, isLoaded, src]);

  return (
    <img
      ref={imageRef}
      key={keyValue}
      style={{ height: isLoaded ? "auto" : "200px" }}
      className={className}
    />
  );
}

LazyImage.propTypes = {
  src: PropTypes.string,
  className: PropTypes.string,
  keyValue: PropTypes.string,
};

export default LazyImage;