import "./NotFound.css"

export default function NotFound() {
  return (
    <div className="noFoundContainer">
      <h1>There's nothing here!</h1>
    </div>
  );
}
