import { Fragment, useEffect, useRef, useState } from "react";

import { useNavigate } from "react-router-dom";

import "./Trending.css";

import { getImageForTrendingTerm, getTrendingData } from "../../helpers/tenor";
import LazyImage from "../LazyImage/LazyImage";
import throttle from "lodash.throttle";

const calculateColumnCount = () => {
  if (window.innerWidth < 992) return 3;
  if (window.innerWidth < 1200) return 4;
  if (window.innerWidth > 1200) return 5;
};

export default function Trending() {
  const [trendingData, setTrendingData] = useState([]);
  const [page, setPage] = useState(1);
  const [currentPages, setCurrentPages] = useState([0, 1, 2, 3, 4]);
  const [pageSize, setPageSize] = useState(5);
  const [totalPageSize, setTotalPageSize] = useState(5);
  const [itemWidth, setItemWidth] = useState("200px");
  const carouselRef = useRef();
  const carouselScrollref = useRef();

  useEffect(() => {
    window.addEventListener("resize", throttle(handleResize, 500));
    return () => window.removeEventListener("resize", handleResize);
  }, [carouselScrollref]);

  const handleResize = () => {
    let carouselWidth = carouselScrollref?.current?.offsetWidth;
    let pageSize = calculateColumnCount();
    setCurrentPages(Array.from(Array(pageSize), (v, i) => i));
    setPageSize(pageSize);
    setPage(1);
    if (carouselScrollref?.current) carouselScrollref.current.scrollLeft = 0;
    let width = carouselWidth / pageSize;
    setItemWidth(`${width}px`);
  };

  let navigate = useNavigate();
  async function fetchData() {
    const response = await getTrendingData({ limit: 50, ar_range: "all" });
    if (response && Array.isArray(response)) {
      setTrendingData(response);
      handleResize();
      let pageSize = calculateColumnCount();
      let totalItem = response.length;
      setTotalPageSize(totalItem / pageSize);
      for (let i = 0; i < totalItem; i++) {
        const data = response[i];
        const imageData = await getImageForTrendingTerm(data);
        setTrendingData((prevData) =>
          prevData.map((item) => {
            return item.id === imageData.id ? imageData : item;
          })
        );
      }
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setCurrentPages(
      Array.from(Array(pageSize), (v, i) => i + (page - 1) * pageSize)
    );
  }, [page]);

  const handleClick = (searchterm) => {
    navigate(`/search/${encodeURIComponent(searchterm)}`, { replace: true });
  };

  const handleNextClick = () => {
    setPage((page) => page + 1);
  };

  const handlePrevClick = () => {
    setPage((page) => page - 1);
  };
  if (!Array.isArray(trendingData) || !(trendingData.length > 0))
    return <div />;
  return (
    <Fragment>
      <div className="carousel">
        <h2 className="mt-4 mb-3">Trending Tenor Searches</h2>
        {page !== totalPageSize ? (
          <div
            className="carouselNavBtn carouselRightBtn"
            onClick={handleNextClick}
          >
            {">"}
          </div>
        ) : (
          ""
        )}

        {page !== 1 ? (
          <div
            className="carouselNavBtn carouselLeftBtn"
            onClick={handlePrevClick}
          >
            {"<"}
          </div>
        ) : (
          ""
        )}
        <div className="carouselScrollable" ref={carouselScrollref}>
          <div
            className="carouselDiv"
            ref={carouselRef}
            style={
              pageSize > 3 ? { transform: `translateX(-${page - 1}00%)` } : {}
            }
          >
            {trendingData.map((trendingItem, index) => {
              const { imageSrc, trendingTerm, id } = trendingItem || {};
              return (
                <div
                  key={`TRENDING_DATA_${id}`}
                  className={`trendingItem ${
                    currentPages.includes(index) ? "activeCarousel" : ""
                  } ${
                    index === currentPages[0]
                      ? "firstActiveCarousel"
                      : index === currentPages[currentPages.length - 1]
                      ? " lastActiveCarousel"
                      : ""
                  }`}
                  style={pageSize > 3 ? { flex: `0 0 ${100 / pageSize}%` } : {}}
                  role="button"
                  onClick={() => handleClick(trendingTerm)}
                >
                  <div style={{ maxWidth: pageSize > 3 ? itemWidth : "400px" }}>
                    <LazyImage
                      src={imageSrc}
                      alt={trendingTerm}
                      className="trendingImg"
                    />
                    <div className="trendingSearchLabelDiv">
                      <span className="trendingSearchLabel">
                        {trendingTerm}
                      </span>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
