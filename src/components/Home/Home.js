import { Fragment, useEffect, useState } from "react";

import Trending from "../Trending/Trending";
import SearchHeader from "../SearchHeader/SearchHeader";
import ScrollPagination from "../ScrollPagination/ScrollPagination";

import "./Home.css";

import throttle from "lodash.throttle";
import { getFeaturedData } from "../../helpers/tenor";
import prepareData from "../../helpers/prepareData";
import { calculateColumnCount } from "../../helpers/calculate";

export default function Home() {
  const [results, setResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([[], [], [], []]);
  const [page, setPage] = useState(1);
  const [columnCount, setColumnCount] = useState(calculateColumnCount());
  const [nextPositioin, setNextPositioin] = useState();

  useEffect(() => {
    if((page === 1) || nextPositioin )  fetchData();
  }, [page]);
  const fetchData = async () => {
    setIsLoading(true);
    const response = await getFeaturedData({ limit: 50, pos: nextPositioin });
    const result = response.results;
    setResults((oldValue) =>
      nextPositioin ? [...oldValue, ...result] : result
    );
    const data = prepareData(result, columnCount);
    const [column1, column2, column3, column4] = data || [];
    setData((prevResult) =>
      nextPositioin
        ? [
            [...prevResult[0], ...column1],
            [...prevResult[1], ...column2],
            [...prevResult[2], ...column3],
            [...prevResult[3], ...column4],
          ]
        : [column1, column2, column3, column4]
    );
    setNextPositioin(response.next);
    setIsLoading(false);
  };

  function loadMore() {
    setPage((prevPage) => prevPage + 1);
  }

  useEffect(() => {
    const data = prepareData(results, columnCount);
    setData(data);
  }, [columnCount]);

  useEffect(() => {
    window.addEventListener("resize", throttle(handleResize, 500));
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handleResize = () => {
    let columnCount = calculateColumnCount();
    setColumnCount(columnCount);
  };

  return (
    <Fragment>
      <SearchHeader />
      <div className="container">
        <Trending />
        <ScrollPagination
          results={data}
          isFeatured={true}
          loadNext={loadMore}
          page={page}
        />
        {isLoading ? <span className="text-center"><img className="loadingGif" alt="Loading..." src="/loading.gif" /></span> : ""}
      </div>
    </Fragment>
  );
}
