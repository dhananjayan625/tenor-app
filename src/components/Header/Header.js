import { Fragment } from "react";
import { Link } from "react-router-dom";

import "./Header.css";

export default function Header() {
  return (
    <Fragment>
      <nav>
        <div className="container">
          <Link to="/" className="navLogo">
            tenor
          </Link>
        </div>
      </nav>
    </Fragment>
  );
};