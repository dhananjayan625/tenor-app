import { useEffect, useRef, useState } from "react";

import { Link, useNavigate, useParams } from "react-router-dom";

import "./SearchHeader.css";

export default function SearchHeader() {
  const params = useParams();
  const searchTerm = params.searchTerm;
  const inputRef = useRef();
  let navigate = useNavigate();

  const [queryValue, setQueryValue] = useState("");
  const searchHeaderRef = useRef();
  const [isReachedTop, setIsReachedTop] = useState(false);

  const handleObserver = (entries) => {
    const [entry] = entries;
    setIsReachedTop(entry.intersectionRatio < 1);
  };

  useEffect(() => {
    const observer = new IntersectionObserver(handleObserver, {
      threshold: [1],
    });
    if (searchHeaderRef.current) observer.observe(searchHeaderRef.current);

    return () => {
      if (searchHeaderRef.current) observer.unobserve(searchHeaderRef.current);
    };
  }, [searchHeaderRef]);

  useEffect(() => {
    inputRef.current.value = searchTerm || "";
    setQueryValue(searchTerm || "");
  }, [searchTerm]);

  const handleQueryChange = () => [setQueryValue(inputRef.current.value)];

  const redirectToSearch = () => {
    navigate(`/search/${inputRef.current.value}`);
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      redirectToSearch();
    }
  };

  return (
    <div
      className={` searchHeader ${isReachedTop ? "fixedSearchForm" : ""}`}
      ref={searchHeaderRef}
    >
      <div className={`${(window.innerWidth < 576) ? "" : "container"}`}>
        <div className="searchHeaderDiv">
          <Link to="/" className="navLogo text-white">
            tenor
          </Link>
          <form className={"search-form navForm"}>
            <input
              type="text"
              name="query"
              ref={inputRef}
              className="searchInput"
              onChange={handleQueryChange}
              onKeyDown={handleKeyDown}
              value={queryValue}
              placeholder="Search for GIFs"
            />
            <span
              className="search-icon"
              role="button"
              onClick={redirectToSearch}
            >
              <img src="/searchIcon.png" width={18} />
            </span>
          </form>
        </div>
      </div>
    </div>
  );
}
