import { Fragment, useEffect, useState } from "react";

import { useParams } from "react-router-dom";

import SearchHeader from "../SearchHeader/SearchHeader";
import ScrollPagination from "../ScrollPagination/ScrollPagination";

import "./Search.css";

import { getGifBySearchTerm } from "../../helpers/tenor";
import prepareData from "../../helpers/prepareData";
import throttle from "lodash.throttle";

const calculateColumnCount = () => {
  if (window.innerWidth < 992) return 2;
  if (window.innerWidth < 1200) return 3;
  if (window.innerWidth > 1200) return 4;
};

export default function New() {
  const params = useParams();
  const searchTerm = params.searchTerm;
  const [results, setResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([[], [], [], []]);
  const [columnCount, setColumnCount] = useState(calculateColumnCount());
  const [page, setPage] = useState(1);
  const [nextPositioin, setNextPositioin] = useState();

  useEffect(() => {
    if (searchTerm != null)
      if (page === 1 || nextPositioin)
        fetchData({ q: searchTerm, limit: 50, pos: nextPositioin });
  }, [page]);

  const fetchData = async (data) => {
    setIsLoading(true);
    const response = await getGifBySearchTerm(data);
    handleGifResponse(response);
  };

  useEffect(() => {
    setNextPositioin(null);
    setData([[], [], [], []]);
    setResults([]);
    if (searchTerm != null) fetchData({ q: searchTerm, limit: 50 });
  }, [searchTerm]);

  function loadMore() {
    setPage((prevPage) => prevPage + 1);
  }

  const handleGifResponse = (response) => {
    let results = response.results;
    setResults((oldValue) =>
      nextPositioin ? [...oldValue, ...results] : results
    );
    const data = prepareData(results, columnCount);
    const [column1, column2, column3, column4] = data || [];
    setData((prevResult) =>
      nextPositioin
        ? [
            [...prevResult[0], ...column1],
            [...prevResult[1], ...column2],
            [...prevResult[2], ...column3],
            [...prevResult[3], ...column4],
          ]
        : [column1, column2, column3, column4]
    );
    setNextPositioin(response.next);
    setIsLoading(false);
  };

  useEffect(() => {
    const data = prepareData(results, columnCount);
    setData(data);
  }, [columnCount]);

  useEffect(() => {
    window.addEventListener("resize", throttle(handleResize, 500));
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handleResize = () => {
    let columnCount = calculateColumnCount();
    setColumnCount(columnCount);
  };

  return (
    <Fragment>
      <SearchHeader />
      <div className="container">
        {searchTerm != null ? (
          <h1 className="searchTermLabel">{searchTerm}</h1>
        ) : (
          ""
        )}
        <ScrollPagination
          results={data}
          isFeatured={false}
          loadNext={loadMore}
        />
        {isLoading ? (
          <span className="text-center">
            <img className="loadingGif" alt="Loading..." src="/loading.gif" />
          </span>
        ) : (
          ""
        )}
      </div>
    </Fragment>
  );
}
